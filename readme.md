# GOBii Jenkins

THis provides files for, builds and runs a preconfigured Jenkins Docker container.

The Jenkins jobs that come with this rely on the scripts found in the `gobii.deploy.bash` repo.

## Quickstart

The following command would quickly get Jenkins running on port 8090 of your host.

See `jenkins_deploy/jenkins-run.sh` for details about the arguments.

```bash
cd jenkins_deploy
bash jenkins-run.sh /data 8090 False release223
```

## Preconfigured Jobs

The Jobs are organized into Builds, Deployments, Hotfixes, and Tools.

### Builds

Automations that should only be used by advanced users in need of creating/pushing new images of several of the GDM tools.

### Deployments

Configurable jobs to be used to initially deploy pieces of the GDM product suite.

### Hotfixes

Configurable jobs to be used to apply updates to pieces of the GDM product suite.

### Tools

Helpful automations to restart services, dump databases, and more.
